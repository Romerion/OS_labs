#include <stdio.h>

void bubble_sort(int*, int);
void swap(int*, int*);

int main(int argc, int *argv[]) {
 int arr[argc-1];
 for(int i=1; i<argc; ++i) {
  sscanf(argv[i], "%d", arr+(i-1));
 }
 bubble_sort(arr, argc-1);
 for(int i=0; i<argc-1; ++i) {
  printf("%d ", arr[i]);
 }
 printf("\n");
 return 0;
}

void bubble_sort(int* arr, int size) {
 int i = 0;
 int swapped;
 do {
  swapped = 0;
  for(int i=1; i<size; ++i) {
   if(arr[i-1] > arr[i]) {
    swap(arr+(i-1), arr+i);
    swapped = 1;
   }
  }
 } while(swapped);
}

void swap(int* a, int* b) {
 int temp = *a;
 *a = *b;
 *b = temp;
}
