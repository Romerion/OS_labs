#include <stdio.h>
#include <stdlib.h>

struct Node {
 int value;
 struct Node* next;
};

struct Linked_list {
 struct Node* head;
 int size;
};

void print_list(struct Linked_list* list);
int insert_node(struct Linked_list* list, int value, int index);
int delete_node(struct Linked_list* list, int index);

int main() {
 struct Linked_list* list = (struct Linked_list*)malloc(sizeof(struct Linked_list));
 printf("Insert 1 at position 0:\n");
 insert_node(list, 1, 0);
 print_list(list);
 // Should return an error
 printf("Insert 2 at position 2:\n");
 insert_node(list, 2, 2);
 print_list(list);
 printf("Insert 2 at position 1:\n");
 insert_node(list, 2, 1);
 print_list(list);
 printf("Insert 3 at position 0:\n");
 insert_node(list, 3, 0);
 print_list(list);
 printf("Delete element at position 0:\n");
 delete_node(list, 0);
 print_list(list);
 printf("Delete element at position 2:\n");
 // Should return an error
 delete_node(list, 2);
 print_list(list);
 printf("Delete element at position 1:\n");
 delete_node(list, 1);
 print_list(list);
 printf("Delete element at position 0:\n");
 delete_node(list, 0);
 print_list(list);
 return 0;
}

void print_list(struct Linked_list* list) {
 // Node which helps enumerate linked list
 int size = list->size;
 struct Node* temp = list->head;
 for(int i=0; i<list->size; ++i) {
  printf("%d ", temp->value);
  temp = temp->next;
 }
 printf("\n");
}

int insert_node(struct Linked_list* list, int value, int index) {
 if (index > list->size) {
  printf("Index is out of range\n");
  return 1;
 }
 struct Node* temp = (struct Node*)malloc(sizeof(struct Node));
 temp->value = value;
 if (index == 0) {
  temp->next = list->head;
  list->head = temp;
  list->size++;
  return 0;
 }
 struct Node* node = list->head;
 // Index begins at least from 1
 int i = 1;
 while (i++ < index) {
  node = node->next;
 }
 temp->next = node->next;
 node->next = temp;
 list->size++;
 return 0;
}

int delete_node(struct Linked_list* list, int index) {
 if (index >= list->size) {
  printf("Index is out of range\n");
  return 1;
 }
 struct Node* node = list->head;
 if (index == 0) {
  list->head = node->next;
  list->size--;
  free(node);
  return 0;
 }
 int i = 1;
 while (i++ < index)
  node = node->next;
 struct Node* temp = node->next;
 node->next = temp->next;
 list->size--; 
 free(temp);
 return 0;
}
