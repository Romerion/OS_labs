#include <signal.h>
#include <stdio.h>

#define TRUE 1

void sighandler() {
	printf("\nSIGINT signal was intercepted.\n");
}

int main() {
	signal(SIGINT, sighandler);
	while(TRUE);
	return 0;
}


