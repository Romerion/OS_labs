#include <signal.h>
#include <stdio.h>

#define TRUE 1

void sigkillhandler() {
	printf("\nSIGKILL signal was intercepted.\n");
}

void sigstophandler() {
	printf("\nSIGSTOP signal was intercepted.\n");
}

void sigusrhandler() {
	printf("\nSIGUSR1 signal was intercepted.\n");
}

int main() {
	signal(SIGKILL, sigkillhandler);
	signal(SIGSTOP, sigstophandler);
	signal(SIGUSR1, sigusrhandler);
	while(TRUE);
	return 0;
}