#include <unistd.h>
#include <string.h>
#include <stdlib.h>
#include <stdio.h>

int main() {
	char s1[] = "Hello! This is the string need to be transferred\n";
	char s2[1024];
 
	int pipefd[2];
	if (pipe(pipefd) == -1) {
		perror("pipe");
		exit(-1);
	}

	write(pipefd[1], s1, strlen(s1)+1);
	close(pipefd[1]);

	read(pipefd[0], s2, strlen(s1)+1);
	close(pipefd[0]);
	printf("%s", s2);
	return 0;
}
