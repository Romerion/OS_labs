#include <stdio.h>
#include <stdlib.h>
#include <signal.h>
#include <sys/types.h>
#include <unistd.h>


#define TRUE 1

int main() {
	pid_t pid;

	pid = fork();

	if (pid == -1) {
		perror("fork");
		exit(-1);
	}

	if (pid == 0) {
		while(TRUE) {
			printf("%s\n", "I'm alive");
			sleep(1);
		}
	} else {
		sleep(10);
		kill(pid, SIGTERM);
		wait(NULL);
	}
	exit(0);
}