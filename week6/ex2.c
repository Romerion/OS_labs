#include <unistd.h>
#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <sys/types.h>

int main() {
 int pipefd[2];
 if (pipe(pipefd) == -1) {
  perror("pipe");
  exit(-1);
 }
 
 pid_t pid;

 pid = fork();
 if (pid == -1) {
  perror("fork");
  exit(-1);
 }

 if (pid != 0) {
  char s[] = "Hello! This is the string need to be transferred\n";
  write(pipefd[1], s, strlen(s)+1);
  wait(NULL);
 }
 else {
  char s[1024];
  read(pipefd[0], s, 1024);
  printf("%s", s);
 }
 close(pipefd[1]);
 close(pipefd[0]);
 exit(0);
}
