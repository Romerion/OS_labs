#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <unistd.h>

#define TRUE 1

// The given description is false. The control does not return to parent process
// if child process was stopped or resumed. It returns only if child process 
// was terminated. Actually, according to manual, by default,
// waitpid() waits only for terminated children, but this behavior is modifiable
// via the options argument. Below there was written waitpid() with needed options arguments.
int main() {
	pid_t pid;
	pid_t pid2;

	int pipefd[2];

	if (pipe(pipefd) == -1) {
		perror("pipe");
		exit(-1);
	}

	printf("%s\n", "New pipe was initialized");

	if ((pid = fork()) == -1) {
		perror("fork");
		exit(-1);
	}

	if (pid == 0) {
		close(pipefd[1]);
		read(pipefd[0], &pid2, sizeof(pid_t));
		close(pipefd[0]);
		sleep(3);
		kill(pid2, SIGSTOP);
		printf("%s\n", "Child process 2 was stopped");
		exit(0);
	} else {
		close(pipefd[0]);
		printf("%s\n", "Child process 1 was forked");
		if ((pid2 = fork()) == -1) {
			perror("fork");
			exit(-1);
		}
		if (pid2 == 0) {
			close(pipefd[0]);
			close(pipefd[1]);
			while(TRUE) {
				printf("%s\n", "I'm the child process 2");
				sleep(1);
			}
			exit(0);
		} else {
			printf("%s\n", "Child process 2 was forked");
			write(pipefd[1], &pid2, sizeof(pid_t));
			printf("%s\n", "Pid of child 2 was sent to child 1");
			close(pipefd[1]);
			printf("%s\n", "Parent process is waiting for child 2");
			// waitpid(pid2, NULL, 0);
			// Added options for trace SIGSTOP and SIGCONT
			waitpid(pid2, NULL, WCONTINUED | WUNTRACED);
			printf("%s\n", "Parent process is here again");
			exit(0);
		}
	}
}
