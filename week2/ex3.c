#include <stdio.h>

void printTriangle(int n);

int main(int argc, char *argv[]) {
    int n;
    sscanf(argv[1], "%d", &n);
    printTriangle(n);
    return 0;
}

void printTriangle(int n) {
    for (int i=0; i<n; ++i) {
        int j = 0;
        while (j < n-i) {
            printf(" ");
            ++j;
        }
        for (j=0; j<2*(i+1)-1; ++j){
            printf("*");
        }
        printf("\n");
    }
}
