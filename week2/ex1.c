#include <stdio.h>
#include <limits.h>
#include <float.h>

int main() {
    int a;
    float b;
    double c;
    a = INT_MAX;
    b = FLT_MAX;
    c = DBL_MAX;
    printf("Max integer size: %lu\nMax integer value: %d\n", sizeof(a), a);
    printf("Max float size: %lu\nMax float value: %f\n", sizeof(b), b);
    printf("Max double size: %lu\nMax double value: %f\n", sizeof(c), c);
    return 0;
}
