#include <stdio.h>

void swapInt(int*, int*);

int main() {
    int number1;
    int number2;
    printf("Write first integer:\n");
    scanf("%d", &number1);
    printf("Write second integer:\n");
    scanf("%d", &number2);
    swapInt(&number1, &number2);
    printf("First number now is: %d\n", number1);
    printf("Second number now is: %d\n", number2);
    return 0;
}

void swapInt(int* first, int* second) {
    int temp = *first;
    *first = *second;
    *second = temp;
}
