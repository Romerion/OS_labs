#include <stdio.h>

int getLine(char s[], int lim);

int main() {
    int lim = 256;
    char s[lim];
    printf("Write anything:\n");
    int i = 0;
    int len = getLine(s, lim);
    for(i=len-1; i>=0; i--) {
        putchar(s[i]);
    }
    putchar('\n');
    return 0;
}

int getLine(char s[], int lim) {
    int c, i;
    for (i=0; i<lim-1 && (c=getchar()) != EOF && c != '\n'; ++i) {
        s[i] = c;
    }
    if (c == '\n') {
        s[i] = c;
        ++i;
    }
    s[i] = '\0';
    return i;
}
