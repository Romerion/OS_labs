#!/bin/bash
if [ ! -f ex2_counter ]; then echo 0 > ex2_counter; fi
touch ex2_counter
for i in {1..100}; do
x=$( tail -n 1 ex2_counter )
y=$(($x+1))
echo $y >> ex2_counter
done

# race condition manifest itself at the very beginning
# critical region is the part of code that should be protected from 
# race condition
