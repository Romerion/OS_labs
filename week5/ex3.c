#include <stdio.h>
#include <pthread.h>

#define N 10
#define TRUE 1
#define THREAD_N 2

int count = 0;
int items[N];
int consumer_sleep = 0;
int producer_sleep = 0;

void insert_item(int item);
int remove_item();
int produce_item();

void* producer() {
 int item;
 while (TRUE) {
  item = produce_item();
  printf("Inserted item: %d\n", item);
  printf("Value: %d\n", count);
  if (count == N) {
   producer_sleep=1;
   while(producer_sleep);
  }
  insert_item(item);
  count = count + 1;
  if (count == 1) consumer_sleep = 0;
 }
 pthread_exit(NULL);
}

void* consumer() {
 int item;
 while (TRUE) {
  if (count == 0) {
   consumer_sleep = 1;
   while (consumer_sleep);
  }
  item = remove_item();
  printf("Remove item: %d\n", item);
  printf("Value: %d\n", count);
  count = count - 1;
  if (count == N-1) producer_sleep = 0;
 }
 pthread_exit(NULL);
}

void insert_item(int item) {
 items[count] = item;
}

int remove_item() {
 return items[count-1];
}

int produce_item() {
 int static item = 0;
 return item++;
}

int main() {
 pthread_t thread_id[THREAD_N];
 int rc;
 rc = pthread_create(&thread_id[0], NULL, producer, NULL);
 if (rc) {
  printf("\n ERROR: return code from pthread_create is %d \n", rc);
  exit(-1);
 }
 rc = pthread_create(&thread_id[1], NULL, consumer, NULL);
 if (rc) {
  printf("\n ERROR: return code from pthread_create is %d \n", rc);
  exit(-1);
 }
 pthread_join(thread_id[0], NULL);
 pthread_join(thread_id[1], NULL);
 return 0;
}
