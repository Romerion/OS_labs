#include <stdio.h>
#include <pthread.h>
#include <stdlib.h>

void* printHello(void* i) {
 printf("Hello from thread number %d\n", i);
 pthread_exit(NULL);
}

int main(int argc, char * argv[]) {
 if (argc < 2) {
  printf("ERROR: write a number of threads to create\n");
  pthread_exit(NULL);
 }
 int n = atoi(argv[1]);
 pthread_t thread_id[n];
 int rc, i;
 printf("Parallel running:\n");
 for (i=0; i<n; ++i) {
  printf("I am main program. I am creating new thread number %d ...\n", i);
  rc = pthread_create(&thread_id[i], NULL, printHello, (void*)i);
  if (rc) {
   printf("\n ERROR: return code from pthread_create is %d \n", rc);
   exit(-1);
  }
 }
 sleep(1);
 printf("\nSequentional running:\n");
 for (i=0; i<n; ++i) {
  printf("I am main program. I am creating new thread number %d ...\n", i);
  rc = pthread_create(&thread_id[i], NULL, printHello, (void*)i);
  if (rc) {
   printf("\n ERROR: return code from pthread_create is %d \n", rc);
   exit(-1);
  }
  pthread_join(thread_id[i], NULL);
 }
 exit(NULL);
}
