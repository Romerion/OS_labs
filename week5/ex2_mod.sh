#!/bin/bash
if [ ! -f ex2_mod_counter ]; then echo 0 > ex2_mod_counter; fi
count=0
touch ex2_mod_counter
while [[ $count != 100 ]]; do
  if ln ex2_mod_counter ex2_mod_counter.lock
  then
   count=$(($count+1))
   n=$(tail -1 ex2_mod_counter)
   echo $(($n + 1)) >> ex2_mod_counter
   rm ex2_mod_counter.lock
  fi
done
