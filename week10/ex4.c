#include <sys/types.h>
#include <stdlib.h>
#include <stdio.h>
#include <dirent.h>
#include <sys/stat.h>
#include <unistd.h>
#include <string.h>

void add_name(char* name, uint inode, char (*names)[256], uint* inodes, int counter) {
	int is_in = 0;
	for (int i=0; i < counter; ++i) {
		if (inodes[i] == inode) {
			is_in = 1;
			strcat(names[i], ", ");
			strcat(names[i], name);
		}
	}
	if (!is_in) {
		strcpy(names[counter], name);
		inodes[counter] = inode;
	}
}

void write_result(char (*names)[256], int counter) {
	FILE* file = fopen("./ex4.txt", "w");
	for (int i=0; i<counter; ++i) {
		int len = strlen(names[i]);
		for (int j=0; j<len; ++j) {
			fputc(names[i][j], file);
		}
		fputc('\n', file);
	}
	fclose(file);
}

int main() {
	char dir_path[] = "./tmp";
	DIR* dirp = opendir(dir_path);
	if (dirp == NULL) { return (1); }

	struct dirent *dp;
	char* name;
	struct stat buf;

	char names[256][256];
	uint i_nodes[256];
	int counter = 0;

	while ((dp = readdir(dirp)) != NULL) {
		if ( strcmp(dp->d_name, ".") && strcmp(dp->d_name, "..")) {
			name = dp->d_name;
			// printf("%s\n", name);
			int len = strlen(dir_path) + strlen(name) + 2;
			char full_name[len];
			strcpy(full_name, dir_path);
			strcat(full_name, "/");
			strcat(full_name, name);
			stat(full_name, &buf);
			if (buf.st_nlink > 1) {
				add_name(name, buf.st_ino, names, i_nodes, counter);
				counter++;
			}
		}
	}
	write_result(names, counter);
	return 0;
}