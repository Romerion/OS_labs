#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int main() {
	setvbuf(stdout, NULL, _IOLBF, BUFSIZ);
	char s[] = "Hello\n";
	for (int i=0; i<strlen(s); ++i) {
		printf("%c", s[i]);	
		sleep(1);
	}
	return 0;
}