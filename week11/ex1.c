#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/mman.h>
#include <string.h>

int main() {
	int fd;
	if ((fd = open("ex1.txt", O_RDWR)) < 0) { 
		perror("open file"); 
		exit(1); 
	}

	struct stat buf;
	if (fstat(fd, &buf)) {
		perror("fstat");
		exit(1);
	}

	off_t fsize = buf.st_size;
	void* addr = mmap(NULL, fsize, PROT_READ | PROT_WRITE, MAP_SHARED, fd, 0);
	close(fd);
	char ans[] = "This is a nice day";
	strcpy((char*)(addr), ans);
	for (int i = strlen(ans); i<fsize; ++i) {
		*(char*)(addr+i) = ' ';
	}
	*(char*)(addr+fsize-1) = '\n';
	munmap(addr, fsize);

	return 0;
}