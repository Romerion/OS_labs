#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/mman.h>
#include <string.h>

int main() {
	int fd1;
	if ((fd1 = open("ex1.txt", O_RDWR)) < 0) { 
		perror("open file ex1.txt"); 
		exit(1); 
	}

	struct stat buf;
	if (fstat(fd1, &buf)) {
		perror("fstat");
		exit(1);
	}

	off_t fsize = buf.st_size;
	void* src = mmap(NULL, fsize, PROT_READ, MAP_SHARED, fd1, 0);
	close(fd1);

	int fd4;
	if ((fd4 = open("ex1.memcpy.txt", O_RDWR)) < 0) { 
		perror("open file ex1.memcpy.txt"); 
		exit(1); 
	}

	ftruncate(fd4, fsize);
	void* dest = mmap(NULL, fsize, PROT_READ | PROT_WRITE, MAP_SHARED, fd4, 0);
	close(fd4);

	memcpy(dest, src, fsize);
	munmap(src, fsize);
	munmap(dest, fsize);

	return 0;
}