#include <stdio.h>

// return 1 if hit and 0 otherwise
int make_clock(int num, int *addresses, int *counters, int pages_count) {
	int found_address = 0;
	int found_free = 0;
	int min = 256;
	int min_page = -1;
	for (int i=0; i<pages_count; ++i) {
		if (addresses[i] == num) {
			counters[i] = ((counters[i] >> 1) + 128) & (255);
			found_address = 1;
		}
		else if ((addresses[i] == -1) && (found_free == 0)) {
			min_page = i;
			min = counters[i];
			// if (found_address == 0)
			// counters[i] = ((counters[i] >> 1) + 128) & (255);
			found_free = 1;
		}
		else if (counters[i] < min) {
			min = counters[i];
			min_page = i;
			counters[i] = (counters[i] >> 1) & (255);
		}
		else {
			counters[i] = (counters[i] >> 1) & (255);
		}
	}
	if (found_address == 1) {
		return 1;
	}
	else {
		addresses[min_page] = num;
		counters[min_page] = ((counters[min_page] >> 1) + 128) & (255);
		return 0;
	}
}

int main(int argc, int *argv[]) {

	if (argc != 3) {
		printf("%s\n", "Wrong number of arguments. Write number of pages and file name as arguments");
	}

	FILE *fp;
	char fname[] = "input.txt";
	sscanf(argv[2], "%s", fname);
	fp = fopen(fname, "r");

	int pages_count;
	sscanf(argv[1], "%d", &pages_count);

	int addresses[pages_count];
	int counters[pages_count];

	for (int i=0; i<pages_count; ++i) {
		addresses[i] = -1;
		counters[i] = 0;
	}

	int num;

	int hit = 0;
	int miss = 0;

	fscanf(fp, "%d", &num);

	while (getc(fp) != '\n') {
		if (make_clock(num, addresses, counters, pages_count) == 0) {
			++miss;
		}
		else {
			++hit;
		}
		fscanf(fp, "%d", &num);	
	}

	if (make_clock(num, addresses, counters, pages_count) == 0) {
		++miss;
	}
	else {
		++hit;
	}

	printf("%s %d\n%s %d\n", "hits:", hit, "misses:", miss);

	return 0;
}
