#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>

#define MAX_PATH_LENGTH 200
#define MAX_COMMAND_LENGTH 200
#define TRUE 1

int getLine(char s[], int lim);

int main() {
 char path[MAX_PATH_LENGTH];
 char command[MAX_COMMAND_LENGTH];
 pid_t pid;
 while (TRUE) {
  printf("%s$ ", getcwd(path, MAX_PATH_LENGTH));
  getLine(command, MAX_COMMAND_LENGTH);
  pid = fork();
  if (pid == 0) {
   char *temp[] = {"sh", "-c", command, NULL};
   execve("/bin/sh", temp, NULL);
   exit(0);
  }
  else if (pid == -1) {
   perror("fork:");
   exit(1);
  }
  else {
   wait(NULL);  
  }
 }
 return 0;
}

int getLine(char s[], int lim) {
    int c, i;
    for (i=0; i<lim-1 && (c=getchar()) != EOF && c != '\n'; ++i) {
        s[i] = c;
    }
    if (c == '\n') {
        s[i] = c;
        ++i;
    }
    s[i] = '\0';
    return i;
}
