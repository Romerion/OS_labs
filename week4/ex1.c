#include <errno.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>

int main() {
 pid_t n;
 switch(n = fork()) {
 case -1:
  perror("fork");
  exit(1);

 case 0:
  printf("Hello from child [PID - %d]\n", getpid());
  exit(0);

 default:
  printf("Hello from parent [PID - %d]\n", getpid());
  wait(NULL);
 }
 return 0;
}
