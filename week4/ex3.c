#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>

#define MAX_PATH_LENGTH 200
#define MAX_COMMAND_LENGTH 200
#define TRUE 1

int main() {
 char path[MAX_PATH_LENGTH];
 char command[MAX_COMMAND_LENGTH];
 while (TRUE) {
  printf("%s$ ", getcwd(path, MAX_PATH_LENGTH));
  scanf("%s", command);
  system(command);
 }
 return 0;
}
