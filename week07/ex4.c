#include <stdlib.h>
#include <stdio.h>

#define MIN(x, y) ((x) < (y) ? (x) : (y))

void* _realloc(void *ptr, int last_size, int new_size) {
	if (ptr == NULL) {
		return malloc(new_size);
	}
	int data_size = MIN(last_size, new_size);
	void* new_ptr = malloc(new_size);
	for (int i=0; i<data_size; ++i) {

		*(unsigned char*)(new_ptr+i) = *(unsigned char*)(ptr+i);
	}
	free(ptr);
	return new_ptr;
}

int main() {
	int n1 = 6;
	int n2 = 10;
	int n3 = 3;
	int* a = (int*)_realloc(NULL, 0, n1*sizeof(int));
	int i;
	for (i=0; i<n1; ++i) {
		a[i] = i;
	}
	for (i=0; i<n1; ++i) {
		printf("%d ", a[i]);
	}
	printf("%s\n", "");
	a = _realloc(a, n1*sizeof(int), n2*sizeof(int));
	for (i=0; i<n2; ++i) {
		printf("%d ", a[i]);
	}
	printf("%s\n", "");
	for (i=n1; i<n2; ++i) {
		a[i] = i;
	}
	for (i=0; i<n2; ++i) {
		printf("%d ", a[i]);
	}
	printf("%s\n", "");
	a = _realloc(a, n2*sizeof(int), n3*sizeof(int));
	for (i=0; i<n3; ++i) {
		printf("%d ", a[i]);
	}
	printf("%s\n", "");
	a = _realloc(a, n3*sizeof(int), 0);
	return 0;
}