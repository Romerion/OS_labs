#include <stdio.h>
#include <stdlib.h>

int main() {
	printf("%s\n", "Type the number 'N' below:");
	int N;
	scanf("%d", &N);
	int *array = malloc(N * sizeof(int));
	for (int i=0; i<N; ++i) {
		*(array+i) = i;
	}
	for (int i=0; i<N; ++i) {
		printf("%d ", *(array+i));
	}
	free(array);
	printf("%s\n", "");
	return 0;
}